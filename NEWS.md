# squirrels 0.4.0
add read_data_day_squirrels function

# squirrels 0.3.0
add global help function

# squirrels 0.2.0
Add save_as_csv function to the package

# squirrels 0.1.0
In this new minor version, we added the `study_activity` function which is focused on the activities of the squirrels given a primary fur color

# squirrels 0.0.1


# squirrels 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.
