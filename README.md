
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrels <img src="man/figures/squirrels_hex.png" align="right" alt="" width="120" />

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrels is to understand the life of squirrels:) This
package was developped during te N2 staging. The real aim of this
package is to increase our capabilities to build a new package

## Installation

You can install the development version of squirrels like so:

``` r
remotes::install_local(path = "~/squirrels_0.2.0.tar.gz")
```

## Example

This is a basic example which shows you how to solve a common problem:
You want to know if your dataset get a variable named
“primary_fur_color”.

First, you have to install the package and then you have to load your
package with the `library` function:

``` r
library(squirrels)
```

With the `get_message_fur_color`, you will have a message on the colorof
study:

``` r
get_message_fur_color(primary_fur_color="Cinnamon")
#> We will interessed on Cinnamon squirrels.
```

You can check if a color is a good color

``` r
squirrels::check_primary_color_is_ok("Gray")
#> [1] TRUE
```
