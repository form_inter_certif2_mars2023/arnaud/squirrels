---
title: "Development actions history"
output: html_document
editor_options: 
  chunk_output_type: console
---

All commands that you use to use when developing packages...

# First time just after creating the project

- Fill the following chunk to create the DESCRIPTION of your package

```{r description, eval=FALSE}
# Describe your package
fusen::fill_description(
  pkg = here::here(),
  fields = list(
    Title = "Describe a dataset of squirrels",
    Description = "This package will produce some plots, table,...",
    `Authors@R` = c(
      person("Arnaud", "Milet", email = "arnaud.milet@d-sidd.com", role = c("aut", "cre"), comment = c(ORCID = "0000-0002-4269-4093")),
      person(given = "d-sidd", role = "cph")
    )
  )
)
# Define License with use_*_license()
usethis::use_mit_license("Arnaud Milet")
```

# Start using git

```{r, eval=FALSE}
usethis::use_git()
# Deal with classical files to ignore
usethis::git_vaccinate()
# Use main for primary branch
usethis::git_default_branch_rename()
```

# Set extra sources of documentation

```{r, eval=FALSE}
# Install a first time
remotes::install_local()
# README
usethis::use_readme_rmd()
# Code of Conduct
usethis::use_code_of_conduct("arnaud.milet@d-sidd.com")
# NEWS
usethis::use_news_md()

#Version
usethis::use_version(which ="patch" )
```

**From now, you will need to "inflate" your package at least once to be able to use the following commands. Let's go to your flat template, and come back here later if/when needed.**


# Package development tools
## Use once

```{r, eval=FALSE}
# Pipe
usethis::use_pipe()

# package-level documentation
usethis::use_package_doc()

# GitHub
# Add your credentials for GitHub
gitcreds::gitcreds_set()
# Send your project to a new GitHub project
usethis::use_github()

# Set Continuous Integration
# _GitHub
usethis::use_github_action_check_standard()
usethis::use_github_action("pkgdown")
usethis::use_github_action("test-coverage")
# _GitLab
gitlabr::use_gitlab_ci(type = "check-coverage-pkgdown")

# Add new flat template
fusen::add_flat_template("add")
```

## Use everytime needed

```{r}
# Simulate package installation
pkgload::load_all()

# Generate documentation and deal with dependencies
attachment::att_amend_desc()

# Check the package
devtools::check()
```

# Share the package

```{r}
# set and try pkgdown documentation website
usethis::use_pkgdown()
pkgdown::build_site(override = list(destination = "inst/site"))

# build the tar.gz with vignettes to share with others
devtools::build(vignettes = TRUE)
```

# code coverage

```{r}
# Pour réaliser un test de code coverage en local, il faut utiliser la fonction package_coverage() du package {covr}. 
covr::package_coverage()


#Raccourci : quelles sont les parties du code qui ne sont pas couvertes du tout ? 
my_coverage <- covr::package_coverage()
covr::zero_coverage(my_coverage)

#Pour un rapport du code coverage
covr::report()
```

# Les données


```{r}
# Jeux de données internes, accessible uniquement aux développeurs
data_act_squirrels <- readr::read_csv("data-raw/nyc_squirrels_act_sample.csv") %>%
  head(15)

#Jeux de données exporté, disponible à l'utilisateur
usethis::use_data(data_act_squirrels, overwrite = TRUE)

checkhelper::use_data_doc("data_act_squirrels")
rstudioapi::navigateToFile("R/doc_data_act_squirrels.R")
```
# Quelques commandes git

```
git log --oneline
git cherry-pick b7d321c

git checkout main
```

