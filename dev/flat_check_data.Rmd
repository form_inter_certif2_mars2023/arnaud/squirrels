---
title: "flat_check_data.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# check_primary_color_is_ok
    
```{r function-check_primary_color_is_ok}
#' Check the values of primary fur color
#'
#' @param string Character. A vector with the primary fur color.
#'
#' @return Boolean. TRUE if all colors are correct.
#' @export
#'
#' @examples
check_primary_color_is_ok <- function(string) {
  all_colors_OK <- all(
    string %in% c("Gray", "Cinnamon", "Black", NA)
  )
  if (isFALSE(all_colors_OK)|is.null(string) ){
    stop("This color is not a good color!")
  }
  return(all_colors_OK)
}
```
  
```{r example-check_primary_color_is_ok}
check_primary_color_is_ok("Gray")
```
  
```{r tests-check_primary_color_is_ok}
test_that("check_primary_color_is_ok works", {
  expect_true(inherits(check_primary_color_is_ok, "function")) 
  expect_true(check_primary_color_is_ok("Gray"))
  expect_true(check_primary_color_is_ok(c("Gray","Cinnamon","Black")))
})

test_that("checks_primary_color_error",{
  expect_error(
    object = check_primary_color_is_ok("Gris")
    )
  expect_error(
    object = check_primary_color_is_ok(NULL)
    )
})


```
  
# check_squirrel_data_integrity
    
```{r function-check_squirrel_data_integrity}
#' A function to verify data integrity
#' 
#' This function will check:
#'  - if the data contains a primary_fur_color column
#'  - if the primary_fur_color contains allowed colors
#' @param data_squirrels dataframe
#' @return A message if the set of data is corect 
#' 
#' @export
check_squirrel_data_integrity <- function(data_squirrels) {
  
  test_contain_color_col <-
    "primary_fur_color" %in% colnames(data_squirrels)
  
  if (isFALSE(test_contain_color_col)) {
    stop("Data must contain a column named 'primary_fur_color'")
  }
  
  check_primary_color_is_ok(string = unique(data_squirrels$primary_fur_color))
  
  return(message("Everything in the data is ok"))
  
}
```
  
```{r example-check_squirrel_data_integrity}
my_data_squirrels <- readr::read_csv(system.file("nyc_squirrels_sample.csv",package = "squirrels"))

check_squirrel_data_integrity(data_squirrels = my_data_squirrels)
```
  
```{r tests-check_squirrel_data_integrity}

  my_data_squirrels <- readr::read_csv(system.file("nyc_squirrels_sample.csv",package = "squirrels"))

test_that("check_squirrel_data_integrity works", {

  expect_true(inherits(check_squirrel_data_integrity, "function")) 
  expect_message(
    object = check_squirrel_data_integrity(data_squirrels = my_data_squirrels),
    regexp="Everything in the data is ok"
  )
  
  expect_error(
    object = check_squirrel_data_integrity(data_squirrels = iris),
    regexp="Data must contain a column named 'primary_fur_color'"
  )
  expect_error(
    object = check_squirrel_data_integrity(data_squirrels = iris %>% dplyr::rename(primary_fur_color=Species)),
    regexp = "This color is not a good color!"
  )
  expect_error(
    object = check_squirrel_data_integrity(1)
  )

})

test_that("check_squirrel_data_integrity don't works", {
  expect_error(
    object = check_squirrel_data_integrity(1)
  )
})
```
  
```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_check_data.Rmd", 
               vignette_name = "Check data")
```

