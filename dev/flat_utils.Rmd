---
title: "flat_utils.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# launch_help

```{r function-launch_help}
#' launch_help Title
#'
#' @importFrom utils browseURL
#' @return open the website
#' @export
#'
#' @examples
launch_help <- function() {
  system.file("site", "index.html", package = "squirrels") %>%
    browseURL()
}
```

```{r examples-launch_help}
launch_help()
```

```{r tests-launch_help}
test_that("launch_help works", {

})
```


```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_utils.Rmd", vignette_name = "Help")
```

